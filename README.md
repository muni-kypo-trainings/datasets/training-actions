# Training Actions of Participants of Hands-on Cybersecurity Training

This repository contains 432 training actions from 10 trainees who participated in cybersecurity training.

The training actions were collected from the open-source training [Secret Laboratory](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory). The training was deployed in [KYPO Cyber Range Platform](https://crp.kypo.muni.cz/), and the format of the actions is documented [here](https://docs.crp.kypo.muni.cz/extras/logging/logs/#training-events-linear). The training participants were computer science graduate students or graduates, who attended the training remotely in June/July 2021.

## Related materials

* [Command-line logs](https://gitlab.ics.muni.cz/muni-kypo-trainings/datasets/commands) from the training

## Credits

[Cybersecurity Laboratory](https://cybersec.fi.muni.cz)\
Faculty of Informatics\
Masaryk University

[CSIRT-MU](https://csirt.muni.cz)\
Institute of Computer Science\
Masaryk University